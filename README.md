####Java代码规范

1. [Google Java编程风格指南(中文翻译)](http://www.hawstein.com/posts/google-java-style.html)  

2. [Google Java编程风格指南(英文原版)](https://google.github.io/styleguide/javaguide.html) - 需翻墙

---
####项目构建工具
java项目约定采用[Gradle](http://gradle.org/)构建，初次接触gradle者可参考以下资源快速学习：  

1. [Gradle 10分钟上手指南](http://www.cnblogs.com/yjmyzz/p/gradle-getting-start.html)
2. [hello-gradle](https://github.com/yjmyzz/hello-gradle)

---
####源代码管理
采用git管理源代码，回家么所有项目托管在[bitbucket.org](https://bitbucket.org/)，新入职的程序员先向相关管理员申请加入RRTL team。项目的git分支管理，约定使用以下二个分支：

1. master分支，用于向生产环境正式部署，一般不直接在该分支上开发，除非生产环境有bug。
2. dev分支，用于其它环境及各种新功能的开发，测试通过后需人工合并到master分支。

---
####项目采用的主要开源框架
1. [spring](http://spring.io/) (用于对象注入、AOP、整合其它开源框架之类)
2. [thrift](http://thrift.apache.org/)（用于php api与java service之间的rpc通讯）
3. [mybatis](https://github.com/mybatis/mybatis-3) （用于操作数据库）
4. 其它一些涉及到的开源框架有[quartz](http://www.quartz-scheduler.org/)（定时任务调度）,[rabbit MQ](http://www.rabbitmq.com/)（消息对象），[mongodb](http://www.quartz-scheduler.org/)...

---
####配置文件约定

1、java项目的配置文件约定不打包到jar包内，统一放在项目启动脚本同级的相对目录下，类似以下结构：  

```
├── resources
│   ├── log4j2.xml
│   ├── mybatis-config.xml
│   ├── properties
│   │   ├── jdbc.properties
│   │   ├── jms.properties
│   │   └── ...
│   ├── spring-context.xml
│   ├── spring-database.xml
│   ├── spring-quartz.xml
│   ├── spring-rabbit.xml
│   └── ...
└── run.sh
```
这样便于在服务器上直接调整配置文件中的相关参数。  
  
2、配置按模块划分多个文件

  a. spring-context.xml 约定*-context.xml为入口配置文件(使用import导入其它子模块配置文件)  
  

  b. spring-database.xml 约定为数据库相关的配置文件  
  

  c. spring-quartz.xml 约定为定时任务的配置文件  

  d. spring-rabbit.xml 约定为rabbitmq的配置文件  

  e. 其它以此类推... 

---

####项目部署
目前暂由各java项目开发人员自行编写"一键部署"shell命令行脚本，该脚本的主要处理流程如下：

1. 调用gradle编译项目
2. 复制各环境的配置文件到输出目录
3. 调用tar压缩
4. 调用scp上传到远程服务器
5. 调用ssh远程解压
6. 清理远程服务器上的无用文件（比如：旧日志，tar包）- 可选
7. 调用ssh执行远程服务器上的部署shell，kill掉旧进程，重启应用

具体可参考[huijiame_order](https://bitbucket.org/rrlt/huijiame_order/src/)项目根目录下的build.sh 

---
####现有Java项目：

由于我们没有java旧项目的历史负担，jdk约定采用较新的1.8版本，以方便将来与时俱进。

1. [huijiame_common_utils](https://bitbucket.org/rrlt/huijiame_common_utils) java项目的通用工具包
2. [huijiame_order](https://bitbucket.org/rrlt/huijiame_order) 订单/活动/库存后台服务
3. [huijiame_thrift_contract](https://bitbucket.org/rrlt/huijiame_thrift_contract) thrift IDL 服务契约
4. [huijiame_spider](https://bitbucket.org/rrlt/huijiame_spider) 房天下的小区数据爬虫项目(基于github上的webmagic框架)
5. [webmagic](https://github.com/yjmyzz/webmagic) 原始的webmagic日志采用的是log4j，与回家么其它java项目采用的log4j2不太匹配，故直接在github上fork了一份，将日志模块升级为log4j2
6. [huijiame_zhifu](https://bitbucket.org/rrlt/huijiame_zhifu) 支付项目
7. [huijiame_notify](https://bitbucket.org/rrlt/huijiame_notify) 消息推送

---
####一些改进开发效率的小工具：

1. [lombok](https://projectlombok.org/) 采用注解方式在编译期自动生成setter/getter/toString()等常用方法，大幅减少手写代码行数，支持eclipse、idea，完全免费，推荐使用。idea下的使用方式可参考[lombok在IntelliJ IDEA下的使用](http://www.cnblogs.com/yjmyzz/p/lombok-with-intellij-idea.html) 

2. [Mybatis Plugin for IntelliJ IDEA](https://www.codesmagic.com/) idea下的mybatis插件，可在mapper与xml配置文件之间自动定位，包括自动生成mybatis代码，推荐使用idea的java程序员使用(注：该插件最新版本已经商业化收费)



 
